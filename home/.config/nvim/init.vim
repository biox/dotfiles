" Do not emulate vi
" Plugins
call plug#begin('~/.config/nvim/plugged')
Plug 'jiangmiao/auto-pairs'
Plug 'SirVer/ultisnips'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'airblade/vim-gitgutter'
Plug 'fatih/vim-go'
Plug 'hashivim/vim-terraform'
Plug 'honza/vim-snippets'
Plug 'justinmk/vim-sneak'
Plug 'kien/ctrlp.vim'
Plug 'ludovicchabant/vim-gutentags'
Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-commentary'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-git'
Plug 'tpope/vim-markdown'
Plug 'tpope/vim-rbenv'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-unimpaired'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'vim-ruby/vim-ruby'
Plug 'w0rp/ale'
call plug#end()

if !exists("g:os")
  if has("win64") || has("win32") || has("win16")
    let g:os = "Windows"
  else
    let g:os = substitute(system('uname'), '\n', '', '')
  endif
endif

" General settings {{{
set nowritebackup
set noswapfile
set autowrite

set hidden
set autoread
set history=1000
set backspace=indent,eol,start

set wildmenu
set wildmode=longest,full
set wildignore+=tmp,.bundle,.sass-cache,.git,.svn,.hg,doc,coverage,vendor,node_modules,deps

set scrolloff=3
set splitright
set splitbelow

set t_vb=
set ttyfast
set lazyredraw
set timeoutlen=500
set updatetime=250
" }}}

" Status line {{{
set laststatus=2
set showcmd
set modelines=1
let g:airline_powerline_fonts=0
" }}}

" Colors, formatting and syntax highlighting {{{
if !has('g:syntax_on')|syntax enable|endif
filetype plugin indent on
set background=dark
colorscheme Tomorrow-Night
set cursorline
set nowrap
set synmaxcol=200 " Do not highlight long lines
set softtabstop=2
set tabstop=2
set shiftwidth=2
set shiftround
set expandtab
set autoindent
set smartindent
set nojoinspaces
set number
set numberwidth=4
set fillchars=vert:│
set encoding=utf-8
set list
set listchars=tab:\·\ ,trail:·,eol:¬
" }}}

" Searching {{{
set hlsearch
set incsearch
set ignorecase
set smartcase
set showmatch

if executable("ack")
  set grepprg=ack\ -H\ --nogroup\ --nocolor\ --ignore-dir=tmp\ --ignore-dir=doc
endif
" }}}

" Folding {{{
set foldenable
set foldlevelstart=10
set foldnestmax=10
set foldmethod=indent
" }}}

" Custom key mappings {{{

" Do not jump over 'real' lines, only over screen lines
nnoremap j gj
nnoremap k gk

" Toggle folds with the space bar
nnoremap <space> za

" Simplify window navigation by removing the need for the W key
noremap <C-h> <C-w>h
noremap <C-j> <C-w>j
noremap <C-k> <C-w>k
noremap <C-l> <C-w>l

" Expand %% to the file path in command mode
cnoremap %% <C-R>=expand('%:h').'/'<cr>

let g:mapleader = ","
nnoremap <leader>pp :setlocal paste!<cr>
nnoremap <silent> <leader><cr> :noh<cr>
nmap <leader>w :w!<cr>
nnoremap <leader><leader> <c-^>
nnoremap <leader>h :set invhls<CR>
nnoremap <leader>y "*y
xnoremap <leader>y "*ygv
nnoremap <leader>sc <ESC>/\v^[<=>]{7}( .*\|$)<CR>
nnoremap <leader>sh :%s/\v:(\w+) \=\>/\1:/g<cr> " Replace Ruby 1.8 Hash syntax with 1.9 Hash syntax
nnoremap <leader>gg :topleft :split Gemfile<cr>
nnoremap <leader>gt :topleft :split TODO<cr> " Handy for keeping a TODO list in the project root
nnoremap <leader>i mmgg=G`m
nnoremap <leader>gb :ls<cr>:b<space>
" }}}

" Plugins {{{

" CtrlP {{{
let g:ctrlp_extensions = ['tag']
" }}}

" {{{ Markdown
let g:markdown_fenced_languages = ['ruby', 'go', 'eruby']
" }}}
"
" {{{ Ultisnips
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
" }}}

" {{{ auto-pairs
let g:AutoPairsFlyMode = 1
" }}}
"
" {{{ NerdTree
augroup nerd_tree
  autocmd!
  autocmd StdinReadPre * let s:std_in=1
  autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
augroup END

nnoremap <C-f> :NERDTreeToggle<CR>
let NERDTreeMinimalUI = 1
let NERDTreeDirArrows = 1
let g:NERDTreeIndicatorMapCustom = {
      \  "Modified"  : "*",
      \  "Staged"    : "+",
      \  "Untracked" : "^",
      \  "Renamed"   : ">",
      \  "Unmerged"  : "═",
      \  "Deleted"   : "×",
      \  "Dirty"     : "×",
      \  "Clean"     : "✔︎",
      \  "Ignored"   : "í",
      \  "Unknown"   : "?"
\}
" }}}

" Autocommands {{{

" No Auto-Comments
augroup auto_comment
  autocmd!
  autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
augroup END

function! StripTrailingWhitespace() abort
  silent exe "normal ma<CR>"
  let saved_search = @/
  %s/\s\+$//e
  silent exe "normal `a<CR>"
  let @/ = saved_search
endfunction

if has('autocmd')

  augroup configroup

    " Remove all previously defined autocommands so re-loading this file doesn't
    " re-define everything.
    autocmd!

    " General hooks {{{

    " Automatically strip trailing whitespace from most regular source code file
    " types using a custom function.
    autocmd FileType ruby,html,haml,css,js,vim autocmd BufWritePre <buffer> :call StripTrailingWhitespace()

    " Resize all windows to optimum distribution whenever Vim itself (the
    " terminal window it lives in) is resized.
    autocmd VimResized * wincmd =

    " Disable paste mode when leaving insert mode.
    autocmd InsertLeave * set nopaste

    " When changing this file, always immediately reload it.
    autocmd BufWritePost .vimrc source $MYVIMRC

    " }}}

    " Chef {{{

    autocmd BufNewFile,BufRead */code/chef/cookbooks/* set filetype=ruby.chef

    " }}}

    " Go {{{

    " Automagically manage imports
    let g:go_fmt_command = "goimports"

    " C-n/C-m for moving through syntax problems
    let g:go_list_type = "quickfix"
    map <C-n> :cnext<CR>
    map <C-m> :cprevious<CR>

    " Leader Mappings
    nnoremap <leader>a :cclose<CR>
    autocmd FileType go nmap <leader>r  <Plug>(go-run)
    autocmd FileType go nmap <leader>t  <Plug>(go-test)
    autocmd FileType go nmap <Leader>c <Plug>(go-coverage-toggle)
    autocmd FileType go nmap <leader>b :<C-u>call <SID>build_go_files()<CR>

    "" Functions
    " run :GoBuild or :GoTestCompile based on the go file
    function! s:build_go_files() abort
      let l:file = expand('%')
      if l:file =~# '^\f\+_test\.go$'
        call go#test#Test(0, 1)
      elseif l:file =~# '^\f\+\.go$'
        call go#cmd#Build(0)
      endif
    endfunction

    " }}}


    " Ruby {{{

    " Silly Rubyists with their silly extensions. Treat the whole lot of 'em as
    " Ruby files.
    autocmd BufNewFile,BufRead {Gemfile,Guardfile,Capfile,Rakefile,Thorfile,config.ru,Vagrantfile,*.prawn} set filetype=ruby
    autocmd BufNewFile,BufRead Gemfile.lock,Procfile set filetype=yaml
    autocmd BufNewFile,BufRead *_spec.rb set filetype=rspec.ruby
    autocmd BufNewFile,BufRead *_steps.rb set filetype=rspec.ruby

    " Set up custom Ruby surround mapping to wrap a motion in an expect call or
    " block.
    autocmd FileType ruby let g:surround_{char2nr("x")} = "expect(\r).to"
    autocmd FileType ruby let g:surround_{char2nr("X")} = "expect { \r }.to"

    autocmd Filetype ruby command! -buffer -nargs=* Rubocop call Rubocop(<q-args>)

    " }}}

    " Plain text, Markdown and markup {{{

    " For text and markup files do not show special characters and hard-wrap
    " text. Limit markdown and text files at 80 characters wide.
    autocmd FileType text,markdown,html,xhtml,eruby setlocal wrap linebreak nolist
    autocmd Filetype text,markdown setlocal textwidth=80

    " }}}

    " Other languages {{{

    autocmd Filetype python nnoremap <buffer> <Leader>r :!python %<cr>
    autocmd Filetype sh     nnoremap <buffer> <Leader>r :!%<cr>

    " }}}

  augroup END

endif

" }}}

function! Rubocop(arg) abort
  let oldmakeprg = &l:makeprg
  try
    execute "set makeprg=rubocop\\ " . a:arg . "\\ %"
    make
  finally
    let &l:makeprg = oldmakeprg
  endtry
endfunction


" vim:foldmethod=marker:foldlevel=0
