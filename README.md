# Dotfiles

These are my configuration files Vim, Git, Irb, Ack and more.

## Installation

Got a new Macbook? No prob.

Use the [homesick gem](https://github.com/technicalpickles/homesick) to install and symlink all files to their proper location:

    $ gem install homesick
    $ homesick clone https://gitlab.com/biox/dotfiles.git
    $ homesick symlink dotfiles

Next, install homebrew and install everything important.

    $ cd ~/.homesick/repos/dotfiles
    $ brew bundle

Install all of the vim things

    $ nvim
    :PlugInstall
    :GoInstallBinaries

A _huge_ thanks to [Arjan van der Gaag](https://github.com/avdgaag) for the [original](https://github.com/avdgaag/dotfiles) set of dotfiles I based
mine off of!!

## Neato Shit

### rbenv

Use this to manage ruby environments.

#### Install a new ruby version

`rbenv install 2.4.2`

#### Force a version of ruby to be used in a repo

`echo '2.4.2' > .ruby-version`

### Vim

#### Quick commands (using leader)

I set comma as my leader - this enables fast common operations like saving or
turning on paste mode. Any command listed below needs to have `,` pressed before
it.

- `w` Save file
- `pp` Turn on/off paste mode
- `[return]` Turn off highlighting
